
|                     | GET                     | POST                  |
|---------------------|-------------------------|-----------------------|
| /                   | Get all todos           | -                     |
| todo/               | -                       | Create a new todo     |
| todo/`id`/          | Get todo with `id`      | -                     |
| todo/`id`/update/   | -                       | Update todo with `id` |
| todo/`id`/delete/   | -                       | Delete todo with `id` |
| /todo/json/         | Get done todos as JSON  | -                     |
