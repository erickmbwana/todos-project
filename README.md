## The Todos Project

This is the codebase for the todos video course, an introduction to the Django web framework.
The course is available [on Udemy](https://www.udemy.com/share/100A3gAEMYcFZQTHg=/).

### Installation and set up
Make sure you have Python 3 installed in your system.

Clone the project: `git clone https://gitlab.com/erickmbwana/todos-project.git`

Cd into the project: `cd todos-project`

Install: `pip install -r requirements.txt`

Run the migrations: `python manage.py migrate`

Run the developer server: `python manage.py runserver`

Open your browser and visit `localhost:8000` to access the running site.

If you want access to the code up to the point where we only have the function based views, in the repo do:

`git checkout d46cbed` - the commit just before introducing class based views.

Remember to visit [Lab of coding](https://labofcoding.com) for the latest videos and blog posts on Python, Django, Flask and related stuff.

Happy coding!
