from datetime import datetime

from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView, UpdateView, ListView, DetailView, View
)

from .forms import TodoForm
from .models import Todo


class IndexView(ListView):

    queryset = Todo.objects.all()
    template_name = 'main/index.html'
    context_object_name = 'todos'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['today'] = datetime.now().date()
        ctx['app_name'] = 'Super Todos'
        return ctx


class TodoDetailView(DetailView):

    model = Todo


class TodoCreateView(SuccessMessageMixin, CreateView):

    model = Todo
    form_class = TodoForm
    success_url = reverse_lazy('index')
    success_message = 'Todo has been created successfully.'


class TodoUpdateView(SuccessMessageMixin, UpdateView):

    model = Todo
    form_class = TodoForm
    success_url = reverse_lazy('index')
    success_message = 'Todo has been updated successfully.'


class TodoJsonView(View):

    http_method_names = ['get', ]

    def get(self, request, *args, **kwargs):
        todos = Todo.objects.filter(done=True).values()
        return JsonResponse({'todos': list(todos)})
